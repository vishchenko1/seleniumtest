import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;


public class SeleniumTask {
    public static void main(String[] args) throws InterruptedException {


        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\chromedriver_win32\\chromedriver.exe");
        //System.setProperty("webdriver.gecko.driver", "C:\\Program Files (x86)\\firefoxdriver_win64\\geckodriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://www.amazon.com");

        WebElement searchbox = driver.findElement(By.id("twotabsearchtextbox"));
        searchbox.sendKeys("Java");
        Thread.sleep(1000);
        searchbox.submit();


        List<WebElement> bookNameFieldList = driver.findElements(By.xpath("//*[@id=\"search\"]//div/div[1]/div/div/div[1]/h2/a/span"));
        List<WebElement> authorNameList = driver.findElements(By.xpath("//div/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/div"));
        List<WebElement> ratingList = driver.findElements(By.xpath("//div/div/div/div[2]/div[2]/div/div[1]/div/div/div[2]/div/span[1]/span/a/i[1]/span"));
        List<WebElement> priceList = driver.findElements(By.xpath("//div[2]/div[1]/div/div[1]/div[2]/div/a[1]/span[1]/span[2]"));
        //List<WebElement> bestSeller = driver.findElements(By.className("a-badge-text"));


        ArrayList<Book> books = new ArrayList<Book>();

        for(int i = 0; (i< bookNameFieldList.size()-1); i++){

            Book book = new Book();
            book.setBookname(bookNameFieldList.get(i).getText());
            book.setAuthor(authorNameList.get(i).getText());
            book.setRate(ratingList.get(i).getAttribute("innerHTML"));
            book.setPrice(priceList.get(i).getText());
            books.add(book);
        }

        for(int i = 0; i < books.size(); i++ ){
            System.out.println(books.get(i).getBookname());
            System.out.println(books.get(i).getAuthor());
            System.out.println(books.get(i).getRate());
            System.out.println(books.get(i).getPrice());
        }




        for(int i = 0; i< books.size(); i++){
            if( books.get(i).getBookname().equals("Head First Java, 2nd Edition")){
                System.out.println("---------------------------------------");
                System.out.println("Head First Java, 2nd Edition is in list");
            }
        }
        driver.quit();

    }
}

