public class Book{
    private String bookName;
    private String author;
    private String price;
    private String  rate;
    private boolean bestSeller;


    public void setBookname(String bookname) {
        this.bookName = bookname;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setBestSeller(boolean bestSeller) {
        this.bestSeller = bestSeller;
    }

    public String getBookname() {
        return bookName;
    }

    public String getAuthor() {
        return author;
    }


    public String getRate() {
        return rate;
    }

    public boolean isBestSeller() {
        return bestSeller;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
